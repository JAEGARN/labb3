//
//  ViewController.h
//  Labb3
//
//  Created by Viktor Jegerås on 2015-02-05.
//  Copyright (c) 2015 Viktor Jegerås. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Task.h"
#import "TaskTableViewController.h"
@interface ViewController : UIViewController

@property(nonatomic)NSMutableArray* tasks;

@end

