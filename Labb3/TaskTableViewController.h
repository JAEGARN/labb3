//
//  TaskTableViewController.h
//  Labb3
//
//  Created by Viktor Jegerås on 2015-02-05.
//  Copyright (c) 2015 Viktor Jegerås. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ViewController.h"
@interface TaskTableViewController : UITableViewController
@property(nonatomic)NSMutableArray* tasks;
@end
