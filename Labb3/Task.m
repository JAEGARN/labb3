//
//  Task.m
//  Labb3
//
//  Created by Viktor Jegerås on 2015-02-05.
//  Copyright (c) 2015 Viktor Jegerås. All rights reserved.
//

#import "Task.h"

@implementation Task

-(Task*)getTask{
    return self;
}
    
-(void)setTask:(NSString *)incomingTaskText{
    self.taskText = incomingTaskText;
}

@end
