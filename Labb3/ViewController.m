//
//  ViewController.m
//  Labb3
//
//  Created by Viktor Jegerås on 2015-02-05.
//  Copyright (c) 2015 Viktor Jegerås. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()
@property (weak, nonatomic) IBOutlet UITextField *AddTaskText;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.tasks = [[NSMutableArray alloc] init];
    Task *t1 = [[Task alloc] init];
    Task *t2 = [[Task alloc] init];
    
    [t1 setTask:@"Städa"];
    [t2 setTask:@"Handla"];
    
    [self.tasks addObject:t1];
    [self.tasks addObject:t2];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)AddTaskButton:(id)sender {
    Task *task = [[Task alloc] init];
    [task setTask:self.AddTaskText.text];
    [self.tasks addObject:[task getTask]];
    self.AddTaskText.text = @"";
}

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    TaskTableViewController * taskView = [segue destinationViewController];
    taskView.tasks = self.tasks;
    
    
    
}

@end
