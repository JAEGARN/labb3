//
//  Task.h
//  Labb3
//
//  Created by Viktor Jegerås on 2015-02-05.
//  Copyright (c) 2015 Viktor Jegerås. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Task : NSObject

@property(nonatomic)NSString* taskText;

-(void)setTask:(NSString *)task;
-(Task*)getTask;

@end
